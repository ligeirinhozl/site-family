// custom.js

// Função para alternar o modo claro/escuro
function toggleDarkMode() {
  var body = document.querySelector('body');
  body.classList.toggle('dark-mode');
  const body = document.body;
  

  // Verifica se o modo escuro está ativado ou desativado
  if (body.classList.contains('dark-mode')) {
    document.querySelector('footer').classList.add('dark-mode');
    localStorage.setItem('dark-mode', 'true');
  } else {
    document.querySelector('footer').classList.remove('dark-mode');
    localStorage.setItem('dark-mode', 'false');
  }
}

// Verifica o modo no carregamento da página
document.addEventListener('DOMContentLoaded', function () {
  if (localStorage.getItem('dark-mode') === 'true') {
    document.querySelector('body').classList.add('dark-mode');
    document.querySelector('footer').classList.add('dark-mode');
  }
  
});




